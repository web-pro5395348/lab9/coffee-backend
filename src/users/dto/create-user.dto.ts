export class CreateUserDto {
  email: string;

  password: string;

  fullname: string;

  gender: string;

  roles: string;

  image: string;
}
